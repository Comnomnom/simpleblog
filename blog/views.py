from django.shortcuts import redirect
from django.contrib import auth

from .forms import LoginForm


def logout(request):
    if request.method == 'POST':
        auth.logout(request)
    return redirect('/blog/users')


def login(request):
    if request.POST:
        logform = LoginForm(request.POST)
        if logform.is_valid():
            username = logform.cleaned_data['username']
            password = logform.cleaned_data['password']
            user = auth.authenticate(username=username, password=password)
            if user:
                auth.login(request, user)
    return redirect('/blog/users')
