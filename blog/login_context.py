from django.template.context_processors import request
from django.contrib.auth.forms import AuthenticationForm

from .forms import *

def login_context(request):
    userlog = request.user
    logform = LoginForm()
    return {"userlog": userlog,
            "logform": logform}




