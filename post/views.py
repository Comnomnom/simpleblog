from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib import auth
from django.views.generic.edit import UpdateView
from django.core.exceptions import PermissionDenied

from .models import Post
from .forms import PostForm
# Create your views here.


def all_users(request):
    users = User.objects.all()
    return render(request, 'allusers.html', {"users": users})


def user_by_id(request, user_id):
    posts = Post.objects.filter(author_id=user_id, publish=True)
    return render(request, 'posts.html', locals())


def blog_by_id(request, post_id):
    posts = Post.objects.filter(id=post_id, publish=True)
    return render(request, 'posts.html', locals())


class PostUpdate(UpdateView):
    model = Post
    form_class = PostForm
    template_name = "updatepost.html"

    def get(self, request, **kwargs):
        self.object = Post.objects.get(id=self.kwargs['id'])
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        context = self.get_context_data(object=self.object, form=form)
        return self.render_to_response(context)

    def get_object(self, *args, **kwargs):
        obj = Post.objects.get(id=self.kwargs['id'])
        #if obj.author != self.request.user:
            #raise PermissionDenied()
        return obj

    def post(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            raise PermissionDenied()
        return super().post(request, *args, **kwargs)

    def get_success_url(self):
        return '/blog/{}/'.format(self.object.id)


def post_create(request):
    post = PostForm(request.POST or None)
    user = auth.get_user(request)
    if request.method == "POST" and post.is_valid() and user:
        new_post = post.save(commit=False)
        new_post.author = user
        new_post.save()
        return redirect("/blog/{}".format(new_post.id))
    return render(request, 'createpost.html', locals())
