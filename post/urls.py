from django.urls import path

from .views import *

urlpatterns = [

    path('users/<int:user_id>/', user_by_id, name='get-posts'),
    path('users/', all_users, name='get-users'),
    path('<int:id>/update/', PostUpdate.as_view(), name='update-post'),
    path('<int:post_id>/', blog_by_id, name='get-post'),
    path('create/', post_create, name='create-post'),

]
